[![pipeline status](https://gitlab.eclipse.org/eclipse/xfsc/tsa/golib/badges/main/pipeline.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/golib/-/commits/main)
[![coverage report](https://gitlab.eclipse.org/eclipse/xfsc/tsa/golib/badges/main/coverage.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/golib/-/commits/main)

# golib

Go library with utility packages used in TSA backend services.

## GDPR

[GDPR](GDPR.md)

## Dependencies

[Dependencies](go.mod)

## License
<hr/>

[Apache 2.0 license](LICENSE)
